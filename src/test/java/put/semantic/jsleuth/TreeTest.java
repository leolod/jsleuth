package put.semantic.jsleuth;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import static put.semantic.jsleuth.TreeString.BACK;

public class TreeTest {

    private Tree t0;
    private Tree t1;
    private Tree t2;

    private static final int A = 0;
    private static final int B = 1;
    private static final int C = 2;
    private static final int D = 3;
    private static final int E = 4;

    @Before
    public void setup() {
        t0 = new Tree(0, 0, new int[]{A, C, BACK, B, D, BACK, BACK});
        t1 = new Tree(0, 1, new int[]{B, A, B, BACK, D, BACK, BACK, B, BACK, C, BACK});
        //uwaga, w oryginalnym artykule brakuje jednego BACK po A,C,B,BACK
        t2 = new Tree(0, 2, new int[]{A, C, B, BACK, BACK, E, A, B, BACK, C, D, BACK, BACK, BACK, BACK});
    }

    @Test
    public void isInducedSubtreeTest() {
        /*
         012  345 67
         ACB$$EAB$CD$$$
         ACD$$B$ -> 4 6 7 5
         */
        assertTrue(t2.isInducedSubtree(new TreeString(new int[]{A, C, D, BACK, BACK, B, BACK}), new MatchLabel(4, 6, 7), 5));
        assertFalse(t2.isInducedSubtree(new TreeString(new int[]{A, D, BACK, B, BACK}), new MatchLabel(4, 7), 5));
    }

    @Test
    public void hasChildrenTest() {
        assertTrue(t0.hasChild(0, 1));
        assertTrue(t0.hasChild(0, 2));
        assertFalse(t0.hasChild(0, 3));
        assertTrue(t1.hasChild(0, 1));
        assertFalse(t1.hasChild(0, 2));
        assertFalse(t1.hasChild(0, 3));
        assertTrue(t1.hasChild(0, 4));
        assertTrue(t1.hasChild(1, 2));
        assertTrue(t1.hasChild(1, 3));
        assertFalse(t1.hasChild(1, 4));
    }

    @Test
    public void maxLabelTest() {
        assertEquals(D, t0.getMaxLabel());
        assertEquals(D, t1.getMaxLabel());
        assertEquals(E, t2.getMaxLabel());
    }

    @Test
    public void t0conversion() {
        VerticalTree db = new VerticalTree(5);
        t0.toScopeLists(db);
        assertEquals(1, db.get(A).size());
        assertEquals(1, db.get(B).size());
        assertEquals(1, db.get(C).size());
        assertEquals(1, db.get(D).size());
        assertEquals(0, db.get(E).size());
//        assertEquals(new Scope(0, 3), ((List<ScopeListTriple>) db.get(A).get(t0)).get(0).getScope());
//        assertEquals(new Scope(2, 3), ((List<ScopeListTriple>) db.get(B).get(t0)).get(0).getScope());
//        assertEquals(new Scope(1, 1), ((List<ScopeListTriple>) db.get(C).get(t0)).get(0).getScope());
//        assertEquals(new Scope(3, 3), ((List<ScopeListTriple>) db.get(D).get(t0)).get(0).getScope());
    }

    @Test
    public void t1conversion() {
        VerticalTree db = new VerticalTree(5);
        t1.toScopeLists(db);
        assertEquals(1, db.get(A).size());
        assertEquals(3, db.get(B).size());
        assertEquals(1, db.get(C).size());
        assertEquals(1, db.get(D).size());
        assertEquals(0, db.get(E).size());
//        assertEquals(new Scope(1, 3), ((List<ScopeListTriple>) db.get(A).get(t1)).get(0).getScope());
//        //porzadek jest w kolejnosci konca zakresu
//        assertEquals(new Scope(2, 2), ((List<ScopeListTriple>) db.get(B).get(t1)).get(0).getScope());
//        assertEquals(new Scope(4, 4), ((List<ScopeListTriple>) db.get(B).get(t1)).get(1).getScope());
//        assertEquals(new Scope(0, 5), ((List<ScopeListTriple>) db.get(B).get(t1)).get(2).getScope());
//        assertEquals(new Scope(5, 5), ((List<ScopeListTriple>) db.get(C).get(t1)).get(0).getScope());
//        assertEquals(new Scope(3, 3), ((List<ScopeListTriple>) db.get(D).get(t1)).get(0).getScope());
    }

    @Test
    public void t2conversion() {
        VerticalTree db = new VerticalTree(5);
        t2.toScopeLists(db);
        assertEquals(2, db.get(A).size());
        assertEquals(2, db.get(B).size());
        assertEquals(2, db.get(C).size());
        assertEquals(1, db.get(D).size());
        assertEquals(1, db.get(E).size());
//        assertEquals(new Scope(4, 7), ((List<ScopeListTriple>) db.get(A).get(t2)).get(0).getScope());
//        assertEquals(new Scope(0, 7), ((List<ScopeListTriple>) db.get(A).get(t2)).get(1).getScope());
//        assertEquals(new Scope(2, 2), ((List<ScopeListTriple>) db.get(B).get(t2)).get(0).getScope());
//        assertEquals(new Scope(5, 5), ((List<ScopeListTriple>) db.get(B).get(t2)).get(1).getScope());
//        assertEquals(new Scope(1, 2), ((List<ScopeListTriple>) db.get(C).get(t2)).get(0).getScope());
//        assertEquals(new Scope(6, 7), ((List<ScopeListTriple>) db.get(C).get(t2)).get(1).getScope());
//        assertEquals(new Scope(7, 7), ((List<ScopeListTriple>) db.get(D).get(t2)).get(0).getScope());
//        assertEquals(new Scope(3, 7), ((List<ScopeListTriple>) db.get(E).get(t2)).get(0).getScope());
    }
}
