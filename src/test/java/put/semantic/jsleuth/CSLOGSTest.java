package put.semantic.jsleuth;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class CSLOGSTest {

    private List<String> load(String file) throws IOException {
        List<String> result = new ArrayList<>();
        try (LineNumberReader r = new LineNumberReader(new FileReader(file))) {
            String line;
            while ((line = r.readLine()) != null) {
                result.add(line);
            }
        }
        Collections.sort(result);
        return result;
    }

    @Test
    public void test_1k_40() throws IOException {
        List<Tree> data = Main.load(new FileReader("src/test/resources/cslogs/cslogs.1k.asc"));
        List<FrequentSubtree> frequent = new Sleuth(data, 40, false).computeFrequent();
        List<String> text = new ArrayList<>();
        for (FrequentSubtree f : frequent) {
            text.add(f.toString());
        }
        Collections.sort(text);
        List<String> expected = load("src/test/resources/cslogs/result.1k.40.sleuth");
        assertArrayEquals(expected.toArray(new String[0]), text.toArray(new String[0]));
    }
}
