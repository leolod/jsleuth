package put.semantic.jsleuth;

import org.junit.Test;
import static org.junit.Assert.*;
import static put.semantic.jsleuth.TreeString.BACK;

public class EquivalenceClassTest {

    private static final int A = 0;
    private static final int B = 1;
    private static final int C = 2;
    private static final int D = 3;
    private static final int E = 4;

    @Test
    public void testComparison() {
        //page 7
        TreeString a = new TreeString(new int[]{B, A, B, BACK, D, BACK, BACK, B, BACK, C, BACK});
        TreeString b = new TreeString(new int[]{B, A, B, BACK, D, BACK, BACK, C, BACK, B, BACK});

        assertFalse(EquivalenceClass.lessThan(a, a));
        assertTrue(EquivalenceClass.lessThan(a, b));
        assertFalse(EquivalenceClass.lessThan(b, b));
        assertFalse(EquivalenceClass.lessThan(b, a));
    }
}
