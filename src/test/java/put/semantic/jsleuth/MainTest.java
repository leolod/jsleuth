/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package put.semantic.jsleuth;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author smaug
 */
public class MainTest {

    String text = "0 0 7 1 2 -1 3 4 -1 -1\n"
            + "1 1 11 2 1 2 -1 4 -1 -1 2 -1 3 -1\n"
            + "2 2 15 1 3 2 -1 -1 5 1 2 -1 3 4 -1 -1 -1 -1";

    @Test
    public void loadTest() throws IOException {
        List<Tree> l = Main.load(new StringReader(text));
        assertEquals(3, l.size());
    }
}
