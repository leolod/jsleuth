package put.semantic.jsleuth;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import static put.semantic.jsleuth.TreeString.BACK;

public class TreeStringTest {

    private TreeString c;

    private static final int A = 0;
    private static final int B = 1;
    private static final int C = 2;
    private static final int D = 3;
    private static final int E = 4;

    @Before
    public void setup() {
        //page 9
        c = new TreeString(new int[]{B, A, A, C, BACK, BACK, B, BACK, BACK, A, B, C, C, BACK, BACK, D, BACK, BACK, B, BACK, BACK, A, B, C, C, BACK, BACK, BACK, BACK});
    }

    @Test
    public void testSubstrings() {
        //page 9
        TreeString c5 = new TreeString(new int[]{A, B, C, C, BACK, BACK, D, BACK, BACK, B, BACK});
        TreeString c11 = new TreeString(new int[]{A, B, C, C, BACK, BACK, BACK});

        assertEquals(c5, c.subtreeForPosition(c.findVertexPosition(5)));
        assertEquals(c11, c.subtreeForPosition(c.findVertexPosition(11)));
    }

    @Test
    public void testFindChildrenPositions() {
        assertArrayEquals(new Integer[]{10, 18}, c.findChildrenPositions(5).toArray(new Integer[0]));
        assertArrayEquals(new Integer[]{11, 15}, c.findChildrenPositions(6).toArray(new Integer[0]));
        assertArrayEquals(new Integer[]{22}, c.findChildrenPositions(11).toArray(new Integer[0]));
    }

    //page 8, figure 5
    @Test
    public void prefixExtensionTest() {
        TreeString t = new TreeString(new int[]{C, D, A, BACK, B, BACK, BACK});
        TreeString t0 = new TreeString(new int[]{C, D, A, BACK, B, BACK, BACK, E, BACK});
        TreeString t1 = new TreeString(new int[]{C, D, A, BACK, B, BACK, E, BACK, BACK});
        TreeString t3 = new TreeString(new int[]{C, D, A, BACK, B, E, BACK, BACK, BACK});
        assertEquals(t0, new TreeString(t, new Element(E, 0)));
        assertEquals(t1, new TreeString(t, new Element(E, 1)));
        assertEquals(t3, new TreeString(t, new Element(E, 3)));
//        assertArrayEquals()
    }
}
