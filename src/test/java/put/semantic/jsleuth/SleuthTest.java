package put.semantic.jsleuth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import static put.semantic.jsleuth.TreeString.BACK;

public class SleuthTest {

    private static final int A = 0;
    private static final int B = 1;
    private static final int C = 2;
    private static final int D = 3;
    private static final int E = 4;

    private Tree t0;
    private Tree t1;
    private Tree t2;

    private Sleuth sleuth;

    @Before
    public void setup() {
        t0 = new Tree(0, 0, new int[]{A, C, BACK, B, D, BACK, BACK});
        t1 = new Tree(1, 1, new int[]{B, A, B, BACK, D, BACK, BACK, B, BACK, C, BACK});
        //uwaga, w oryginalnym artykule brakuje jednego BACK po A,C,B,BACK
        t2 = new Tree(2, 2, new int[]{A, C, B, BACK, BACK, E, A, B, BACK, C, D, BACK, BACK, BACK, BACK});
        sleuth = new Sleuth(Arrays.asList(t0, t1, t2), 2, false);
    }

    @Test
    public void test() {
        List<Tree> db = Arrays.asList(
                new Tree(0, 0, new int[]{0, 1, 3, 5, BACK, BACK, 2, BACK, BACK, 4, BACK}),
                new Tree(1, 1, new int[]{0, 1, 2, BACK, 3, 5, BACK, BACK, BACK, 4, BACK}),
                new Tree(2, 2, new int[]{6, 1, 3, 5, BACK, BACK, 2, BACK, BACK, 4, BACK}),
                new Tree(3, 3, new int[]{6, 1, 2, BACK, 3, 5, BACK, BACK, BACK, 4, BACK})
        );
        Sleuth s = new Sleuth(db, 3, false);
        List<FrequentSubtree> frequent = s.computeFrequent();
        List<TreeString> x = new ArrayList<>();
        for (FrequentSubtree f : frequent) {
            assertTrue(f.getSupport() >= 3);
            System.out.println(f);
            x.add(f.getSubtree());
        }
        assertTrue(x.contains(new TreeString(new int[]{1, 2, BACK, 3, BACK})));
        assertFalse(x.contains(new TreeString(new int[]{1, 3, BACK, 2, BACK})));
        assertTrue(x.contains(new TreeString(new int[]{1, 3, 5, BACK, BACK})));
    }
}
