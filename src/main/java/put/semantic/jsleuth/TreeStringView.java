package put.semantic.jsleuth;

import java.util.Iterator;

public class TreeStringView extends AbstractTreeString {

    private final TreeString orig;
    private final int begin, end;

    public TreeStringView(TreeString orig, int begin, int end) {
        this.orig = orig;
        this.begin = begin;
        this.end = end;
    }

    @Override
    public int size() {
        return end - begin;
    }

    @Override
    public int get(int n) {
        return orig.get(n + begin);
    }


}
