package put.semantic.jsleuth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class TreeString extends AbstractTreeString {

    public static final int BACK = Integer.MAX_VALUE;
    public static final TreeString EMPTY = new TreeString();

    private final int[] data;
    private final int numberOfVertices;

    private TreeString() {
        this.data = new int[0];
        this.numberOfVertices = 0;
    }

    public TreeString(int[] data) {
        this.data = data;
        this.numberOfVertices = countVertices();
        int depth = 0;
        for (int l : data) {
            if (l == BACK) {
                depth--;
            } else {
                depth++;
            }
        }
    }

    public TreeString(TreeString prefix, Element key) {
        if (key.getVertex() == Element.NO_VERTEX && prefix.data.length == 0) {
            this.data = new int[]{key.getLabel()};
        } else {
            this.data = new int[prefix.data.length + 2];
            if (key.getVertex() == Element.NO_VERTEX) {
                throw new IllegalArgumentException("Label without vertex");
            }
            if (prefix.data.length == 0) {
                throw new IllegalArgumentException("How to glue anything to empty prefix?");
            }
            int pos = prefix.findVertexPosition(key.getVertex());
            int end = prefix.findEndFromPosition(pos) - 1;
            for (int i = 0; i <= end; ++i) {
                this.data[i] = prefix.data[i];
            }
            this.data[end + 1] = key.getLabel();
            this.data[end + 2] = BACK;
            for (int i = end + 1; i < prefix.data.length; ++i) {
                this.data[i + 2] = prefix.data[i];
            }
        }
        this.numberOfVertices = countVertices();
    }

    private int countVertices() {
        int result = 0;
        for (int i = 0; i < data.length; i++) {
            if (data[i] != BACK) {
                result++;
            }
        }
        return result;
    }

    public int getNumberOfVertices() {
        return numberOfVertices;
    }

    @Override
    public int size() {
        return data.length;
    }

    @Override
    public int get(int n) {
        return data[n];
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TreeString) {
            TreeString other = (TreeString) obj;
            return Arrays.equals(data, other.data);
        } else {
            return super.equals(obj);
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Arrays.hashCode(this.data);
        return hash;
    }

    public int findVertexPosition(int vertex) {
        int begin;
        int orig = vertex;
        for (begin = 0; begin < data.length; ++begin) {
            if (data[begin] != BACK) {
                if (vertex == 0) {
                    break;
                }
                vertex--;
            }
        }
        if (begin >= data.length) {
            throw new IllegalArgumentException(String.format("Vertex %d in %s not found", orig, toString()));
        }
        return begin;
    }

    public int findEndFromPosition(int begin) {
        assert data[begin] != BACK;
        int end;
        int depth = 0;
        for (end = begin; end < data.length; ++end) {
            if (data[end] != BACK) {
                depth++;
            } else {
                depth--;
                if (depth == 0) {
                    break;
                }
            }
        }
        return end;
    }

    public BasicTreeString subtreeForPosition(int begin) {
        int end = findEndFromPosition(begin);
        return new TreeStringView(this, begin, end);
    }

    public List<Integer> findChildrenPositions(int vertex) {
        int begin = findVertexPosition(vertex) + 1;
        List<Integer> result = new ArrayList<>();
        int depth = 0;
        for (int i = begin; i < data.length; ++i) {
            if (data[i] != BACK) {
                if (depth == 0) {
                    result.add(i);
                }
                depth++;
            } else {
                depth--;
                if (depth < 0) {
                    break;
                }
            }
        }
        return result;
    }
}
