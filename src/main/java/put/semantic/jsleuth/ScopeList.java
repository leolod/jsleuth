package put.semantic.jsleuth;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections4.iterators.IteratorIterable;

public class ScopeList {

    public static class Key {

        private final Tree tree;
        private final MatchLabel label;
        private final int hash;

        public Key(Tree tree, MatchLabel label) {
            this.tree = tree;
            this.label = label;
            this.hash = 31 * tree.hashCode() + label.hashCode();
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Key other = (Key) obj;
            if (!Objects.equals(this.tree, other.tree)) {
                return false;
            }
            return Objects.equals(this.label, other.label);
        }

        public Tree getTree() {
            return tree;
        }

        public MatchLabel getMatchLabel() {
            return this.label;
        }

    }

    private final Map<Key, List<Scope>> data = new HashMap<>();

    public void add(Tree tree, MatchLabel label, Scope scope) {
        Key key = new Key(tree, label);
        List<Scope> list = data.get(key);
        if (list == null) {
            list = new ArrayList<>();
            data.put(key, list);
        }
        list.add(scope);
    }

    public void addAll(Tree tree, MatchLabel label, List<Scope> list) {
        assert !list.isEmpty();
        Key key = new Key(tree, label);
        if (data.containsKey(key)) {
            data.get(key).addAll(list);
        } else {
            data.put(key, list);
        }
    }

    public Iterable<Scope> get(Key key) {
        if (data.containsKey(key)) {
            return data.get(key);
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    public Iterable<Scope> get(Tree t, MatchLabel matchLabel) {
        Key key = new Key(t, matchLabel);
        return get(key);
    }

    public Iterable<Map.Entry<Key, List<Scope>>> mapIterator() {
        return data.entrySet();
    }

    public Iterable<Tree> getTrees() {
        return new IteratorIterable<>(new Iterator<Tree>() {

            private final Iterator<Key> i = data.keySet().iterator();

            @Override
            public boolean hasNext() {
                return i.hasNext();
            }

            @Override
            public Tree next() {
                return i.next().getTree();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }

    public int size() {
        int size = 0;
        for (List<Scope> v : data.values()) {
            size += v.size();
        }
        return size;
    }

}
