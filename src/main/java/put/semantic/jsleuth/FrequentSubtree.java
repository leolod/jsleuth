package put.semantic.jsleuth;

public class FrequentSubtree {

    private final TreeString subtree;
    private final int support;

    public FrequentSubtree(TreeString subtree, int support) {
        this.subtree = subtree;
        this.support = support;
    }

    public TreeString getSubtree() {
        return subtree;
    }

    public int getSupport() {
        return support;
    }

    @Override
    public String toString() {
        return String.format("%s - %d", subtree, support);
    }

}
