package put.semantic.jsleuth;

import java.util.Arrays;

public class MatchLabel {

    public static final MatchLabel EMPTY = new MatchLabel();

    private int[] data = null;
    private final MatchLabel head;
    private final int tail;
    private final int length;
    private final int hash;

    private MatchLabel() {
        this.head = null;
        this.tail = 0;
        this.length = 0;
        this.hash = 1;
    }

    public MatchLabel(MatchLabel head, int tail) {
        this.head = head;
        this.tail = tail;
        this.length = head.length + 1;
        this.hash = 31 * head.hash + tail;
    }

    public MatchLabel(int... data) {
        this(data.length > 1 ? new MatchLabel(Arrays.copyOf(data, data.length - 1)) : new MatchLabel(), data[data.length - 1]);
    }

    @Override
    public int hashCode() {
        return hash;
    }

    private boolean internalEquals(MatchLabel l) {
        if (this.tail != l.tail) {
            return false;
        }
        if (this.head == l.head) {
            return true;
        }
        assert this.head != null;
        assert this.length == l.length;
        return this.head.internalEquals(l.head);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MatchLabel other = (MatchLabel) obj;
        if (this.hash != other.hash) {
            return false;
        }
        if (this.length != other.length) {
            return false;
        }
        return internalEquals(other);
    }

    public int size() {
        return length;
    }

    public int get(int n) {
        assert n < length;
        if (data == null) {
            data = new int[length];
            MatchLabel ptr = this;
            for (int i = length - 1; i >= 0; i--) {
                data[i] = ptr.tail;
                ptr = ptr.head;
            }
        }
        return data[n];
    }
}
