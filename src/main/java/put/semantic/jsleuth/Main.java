package put.semantic.jsleuth;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {

    public static List<Tree> load(Reader in) throws IOException {
        List<Tree> result = new ArrayList<>();
        try (LineNumberReader r = new LineNumberReader(in)) {
            String line;
            while ((line = r.readLine()) != null) {
                String[] items = line.split("\\s+");
                int lineNo = r.getLineNumber() + 1;
                int[] values = new int[items.length];
                for (int i = 0; i < values.length; ++i) {
                    values[i] = Integer.parseInt(items[i]);
                    if (i >= 3 && values[i] < 0) {
                        values[i] = TreeString.BACK;
                    }
                }
                int[] string = Arrays.copyOfRange(values, 3, values.length);
                if (string.length != values[2]) {
                    throw new RuntimeException(String.format("Line %d: tree of length %d, declared %d", lineNo, string.length, values[2]));
                }
                result.add(new Tree(values[0], values[1], string));
            }
        }
        return result;
    }

    public static int countGroups(List<Tree> trees) {
        Set<Integer> groups = new HashSet<>();
        for (Tree t : trees) {
            groups.add(t.getGroup());
        }
        return groups.size();
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        String inputFile = null;
        int support = 0;
        double decimalSupport = Double.NaN;
        boolean embedded = true;
        for (int i = 0; i < args.length; ++i) {
            if (null != args[i]) {
                switch (args[i]) {
                    case "-i":
                        inputFile = args[++i];
                        break;
                    case "-s":
                        decimalSupport = Double.parseDouble(args[++i]);
                        break;
                    case "-S":
                        support = Integer.parseInt(args[++i]);
                        break;
                    case "-I":
                        embedded = false;
                        break;
                }
            }
        }
        if (inputFile == null) {
            throw new IllegalStateException("Input file name not specified");
        }
        System.err.printf("Loading trees from file '%s'\n", inputFile);
        List<Tree> trees = load(new FileReader(inputFile));
        int groups = countGroups(trees);
        if (!Double.isNaN(decimalSupport)) {
            support = (int) Math.ceil(groups * decimalSupport);
        } else {
            decimalSupport = ((double) support) / groups;
        }
        System.err.printf("Support is %d (%f)\n", support, decimalSupport);
        Sleuth s = new Sleuth(trees, support, embedded);
        List<FrequentSubtree> frequent = s.computeFrequent();
        for (FrequentSubtree f : frequent) {
            System.out.println(f);
        }
    }
}
