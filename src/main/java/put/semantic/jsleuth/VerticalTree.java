package put.semantic.jsleuth;

import java.util.ArrayList;

public class VerticalTree extends ArrayList<ScopeList> {

    public VerticalTree(int maxLabel) {
        super(maxLabel + 1);
        for (int i = 0; i <= maxLabel; ++i) {
            this.add(new ScopeList());
        }
    }

}
