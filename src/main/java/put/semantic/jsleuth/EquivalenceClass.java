package put.semantic.jsleuth;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EquivalenceClass {

    private final TreeString prefix;
    private final int[] rightmostPath;
    private final Map<Element, ScopeList> elements;

    public EquivalenceClass() {
        this.prefix = TreeString.EMPTY;
        this.rightmostPath = new int[0];
        this.elements = new HashMap<>();
    }

    public EquivalenceClass(EquivalenceClass P, Element x) {
        this.prefix = new TreeString(P.prefix, x);
        this.rightmostPath = Arrays.copyOf(P.rightmostPath, P.rightmostPath.length + 1);
        this.rightmostPath[this.rightmostPath.length - 1] = this.prefix.getNumberOfVertices() - 1;
        this.elements = new HashMap<>();
    }

    public Map<Element, ScopeList> getElements() {
        return elements;
    }

    public TreeString getPrefix() {
        return prefix;
    }

    public void addElement(Element e, ScopeList scopeList) {
        elements.put(e, scopeList);
    }

    public static boolean lessThan(BasicTreeString x, BasicTreeString y) {
        if (y.size() < x.size()) {
            boolean neq = false;
            for (int i = 0; i < y.size(); ++i) {
                if (x.get(i) != y.get(i)) {
                    neq = true;
                    break;
                }
            }
            if (!neq) {
                return true;
            }
        }
        for (int i = 0; i < Math.min(x.size(), y.size()); ++i) {
            if (x.get(i) < y.get(i)) {
                return true;
            } else if (x.get(i) > y.get(i)) {
                return false;
            }
        }
        return false;
    }

    public boolean isCanonical() {
        for (int root : rightmostPath) {
            List<Integer> childrenPositions = prefix.findChildrenPositions(root);
            if (childrenPositions.size() >= 2) {
                BasicTreeString a = prefix.subtreeForPosition(childrenPositions.get(childrenPositions.size() - 2));
                BasicTreeString b = prefix.subtreeForPosition(childrenPositions.get(childrenPositions.size() - 1));
                if (!lessThan(a, b)) {
                    return false;
                }
            }
        }
        return true;
    }

}
