package put.semantic.jsleuth;

import java.util.Iterator;
import static put.semantic.jsleuth.TreeString.BACK;

public abstract class AbstractTreeString implements BasicTreeString {

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof BasicTreeString)) {
            return false;
        }
        BasicTreeString other = (BasicTreeString) obj;
        if (this.size() != other.size()) {
            return false;
        }
        for (int i = 0; i < this.size(); ++i) {
            if (this.get(i) != other.get(i)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        if (size() == 1) {
            return String.format("%d", get(0));
        }
        String result = "";
        String buffer = "";
        for (int l = 0; l < size(); l++) {
            int i = get(l);
            if (i == BACK) {
                buffer += "-1 ";
            } else {
                result += buffer;
                buffer = "";
                result += String.format("%d ", i);
            }
        }
        return result;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {

            int pos = 0;

            @Override
            public boolean hasNext() {
                return pos < size();
            }

            @Override
            public Integer next() {
                return get(pos++);
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
    }
}
