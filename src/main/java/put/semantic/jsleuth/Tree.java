package put.semantic.jsleuth;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import static put.semantic.jsleuth.TreeString.BACK;

public class Tree {

    private final int groupId;
    private final int treeId;
    private final TreeString string;
    private final int maxLabel;

    public Tree(int groupId, int treeId, int[] string) {
        this.groupId = groupId;
        this.treeId = treeId;
        this.string = new TreeString(string);
        this.maxLabel = computeMaxLabel();
    }

    public TreeString getString() {
        return string;
    }

    private int computeMaxLabel() {
        int max = 0;
        for (int i : string) {
            if (i != TreeString.BACK && i > max) {
                max = i;
            }
        }
        return max;
    }

    public int getMaxLabel() {
        return maxLabel;
    }

    @Override
    public String toString() {
        return String.format("T(%d,%d): [%s]", groupId, treeId, StringUtils.join(string));
    }

    public void toScopeLists(List<ScopeList> result) {
        Deque<Integer[]> stack = new ArrayDeque<>();
        int n = 0;
        for (int i = 0; i < string.size(); ++i) {
            if (string.get(i) != TreeString.BACK) {
                stack.push(new Integer[]{string.get(i), n});
                ++n;
            } else {
                assert !stack.isEmpty();
                Integer[] tab = stack.pop();
                int first = tab[1];
                int label = tab[0];
                assert label >= 0 && label <= maxLabel;
                assert result.get(label) != null;
                result.get(label).add(this, MatchLabel.EMPTY, new Scope(first, n - 1));
            }
        }
        while (!stack.isEmpty()) {
            Integer[] tab = stack.pop();
            int first = tab[1];
            int label = tab[0];
            assert label >= 0 && label <= maxLabel;
            assert result.get(label) != null;
            result.get(label).add(this, MatchLabel.EMPTY, new Scope(first, n - 1));
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Tree)) {
            return false;
        }
        Tree other = (Tree) obj;
        return other.groupId == this.groupId && other.treeId == this.treeId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.groupId;
        hash = 67 * hash + this.treeId;
        return hash;
    }

    public int getGroup() {
        return groupId;
    }

    public int getId() {
        return treeId;
    }

    public boolean hasChild(int root, int vertex) {
        int begin = string.findVertexPosition(root) + 1;
        int n = 0;
        for (int i = begin; i < string.size(); ++i) {
            if (string.get(i) != BACK) {
                n++;
                root++;
                if (root == vertex) {
                    return n == 1;
                }
            } else {
                if (n == 0) {
                    break;
                }
                n--;
            }
        }
        return false;
    }

    public boolean isInducedSubtree(TreeString s, MatchLabel vertices, int last) {
        int n = 0;
        Deque<Integer> path = new ArrayDeque<>();
        for (int i = 0; i < s.size(); ++i) {
            if (s.get(i) != TreeString.BACK) {
                if (vertices.size() == n) {
                    path.push(last);
                } else {
                    path.push(vertices.get(n));
                }
                ++n;
            } else {
                assert !path.isEmpty();
                int vertex = path.pop();
                int root = path.peek();
                if (!hasChild(root, vertex)) {
                    return false;
                }
            }
        }
        return true;
    }
}
