package put.semantic.jsleuth;

public class Element {

    public static final int NO_VERTEX = -1;

    private final int label;
    private final int vertex;

    public Element(int _label, int _vertex) {
        label = _label;
        vertex = _vertex;
    }

    public int getLabel() {
        return label;
    }

    public int getVertex() {
        return vertex;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Element)) {
            return false;
        }
        Element other = (Element) obj;
        return other.label == label && other.vertex == vertex;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.label;
        hash = 29 * hash + this.vertex;
        return hash;
    }

    @Override
    public String toString() {
        return String.format("(%s,%s)", label, vertex);
    }

}
