package put.semantic.jsleuth;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import static put.semantic.jsleuth.TreeString.BACK;

public class Sleuth {

    private final List<Tree> db;
    private final List<ScopeList> vdb;
    private final int maxLabel;
    private final int maxGroup;
    private final int minSupport;
    private final boolean embedded;

    public Sleuth(List<Tree> db, int minSupport, boolean embedded) {
        this.db = db;
        this.minSupport = minSupport;
        this.embedded = embedded;
        this.maxLabel = computeMaxLabel();
        this.maxGroup = computeMaxGroup();
        this.vdb = convertToVertical();
    }

    private List<ScopeList> convertToVertical() {
        List<ScopeList> result = new ArrayList<>(maxLabel + 1);
        for (int i = 0; i <= maxLabel; ++i) {
            result.add(new ScopeList());
        }
        for (Tree t : db) {
            t.toScopeLists(result);
        }
        return result;
    }

    private int computeMaxGroup() {
        int max = 0;
        for (Tree t : db) {
            max = Math.max(max, t.getGroup());
        }
        return max;
    }

    private int computeMaxLabel() {
        int max = 0;
        for (Tree t : db) {
            max = Math.max(max, t.getMaxLabel());
        }
        return max;
    }

    private List<Element> computeF1() {
        Set<Integer>[] groupFrequencies = new Set[maxLabel + 1];
        List<Element> result = new ArrayList<>();
        for (int label = 0; label <= maxLabel; ++label) {
            groupFrequencies[label] = new HashSet<>();
        }
        for (Tree t : db) {
            for (int label : t.getString()) {
                if (label != BACK) {
                    Set<Integer> s = groupFrequencies[label];
                    if (s != null) {
                        s.add(t.getGroup());
                        if (s.size() >= minSupport) {
                            result.add(new Element(label, Element.NO_VERTEX));
                            groupFrequencies[label] = null;
                        }
                    }
                }
            }
        }
        return result;
    }

    private ExecutorService executorService;

    private int getNumberOfThreads() {
        return Runtime.getRuntime().availableProcessors();
    }

    public List<FrequentSubtree> computeFrequent() {
        List<Element> f1 = computeF1();
        executorService = Executors.newFixedThreadPool(getNumberOfThreads());
        List<FrequentSubtree> result = computeFk(f1);
        executorService.shutdown();
        executorService = null;
        return result;
    }

    private List<FrequentSubtree> computeFk(List<Element> f1) {
        EquivalenceClass P = new EquivalenceClass();
        for (Element e : f1) {
            P.addElement(e, vdb.get(e.getLabel()));
        }
        List<FrequentSubtree> frequent = new ArrayList<>();
        Deque<EquivalenceClass> eqClasses = new ArrayDeque<>();
        eqClasses.add(P);
        while (!eqClasses.isEmpty()) {
            int n = eqClasses.size();
            P = eqClasses.pop();
            System.err.printf("Queue size: %d, # of elements in current class: %d\n", n, P.getElements().size());
            try {
                eqClasses.addAll(magic(P, frequent));
            } catch (InterruptedException | ExecutionException ex) {
                throw new RuntimeException(ex);
            }
            P.getElements().clear();
        }
        return frequent;
    }

    private List<EquivalenceClass> magic(EquivalenceClass P, List<FrequentSubtree> frequent) throws InterruptedException, ExecutionException {
        boolean cousins = P.getPrefix().size() > 0;//przy liczeniu F2 nie liczymy kuzynów
        int n = P.getElements().size();
        int i = 0;
        boolean debug = !cousins;
        List<EquivalenceClass> next = new ArrayList<>();
        for (Map.Entry<Element, ScopeList> x : P.getElements().entrySet()) {
            if (debug) {
                i++;
                System.err.printf("%d/%d\n", i, n);
            }
            EquivalenceClass Px = new EquivalenceClass(P, x.getKey());
            if (!Px.isCanonical()) {
                continue;
            }
            int support;
            if (embedded) {
                support = computeEmbeddedSupport(x.getValue());
            } else {
                support = computeInducedSupport(Px.getPrefix(), x.getValue());
            }
            if (support >= minSupport) {    //dla embedded to musi zachodzic, dla induced nie koniecznie
                frequent.add(new FrequentSubtree(Px.getPrefix(), support));
                List<Callable<Map<Element, ScopeList>>> tasks = new ArrayList<>();
                for (Map.Entry<Element, ScopeList> y : P.getElements().entrySet()) {
                    assert P.getPrefix().getNumberOfVertices() == Px.getPrefix().getNumberOfVertices() - 1;
                    if (x == y) {
                        continue;
                    }
                    tasks.add(new ExtendPxCallable(Px, x, y, cousins));
                }
                List<Future<Map<Element, ScopeList>>> futures = executorService.invokeAll(tasks);
                for (Future<Map<Element, ScopeList>> f : futures) {
                    assert f.isDone();
                    Map<Element, ScopeList> map = f.get();
                    for (Map.Entry<Element, ScopeList> e : map.entrySet()) {
                        Px.addElement(e.getKey(), e.getValue());
                    }
                }
                next.add(Px);
            }
        }
        return next;
    }

    private class ExtendPxCallable implements Callable<Map<Element, ScopeList>> {

        private final Map.Entry<Element, ScopeList> x, y;
        private final boolean cousins;
        private final int n;

        public ExtendPxCallable(EquivalenceClass Px, Map.Entry<Element, ScopeList> x, Map.Entry<Element, ScopeList> y, boolean cousins) {
            this.n = Px.getPrefix().getNumberOfVertices() - 1;
            this.x = x;
            this.y = y;
            this.cousins = cousins;
        }

        @Override
        public Map<Element, ScopeList> call() throws Exception {
            ScopeList descList = new ScopeList();
            ScopeList cousinList = new ScopeList();
            for (Map.Entry<ScopeList.Key, List<Scope>> xtriple : x.getValue().mapIterator()) {
                MatchLabel matchLabel = null;
                for (Scope xscope : xtriple.getValue()) {
                    ArrayList<Scope> descListHelper = new ArrayList<>();
                    ArrayList<Scope> cousinListHelper = new ArrayList<>();
                    for (Scope yscope : y.getValue().get(xtriple.getKey())) {
                        if (xscope.contains(yscope)) {
                            descListHelper.add(yscope);
                        } else if (cousins && (xscope.before(yscope) || yscope.before(xscope))) {
                            cousinListHelper.add(yscope);
                        }
                    }
                    if (!descListHelper.isEmpty() || !cousinListHelper.isEmpty()) {
                        matchLabel = new MatchLabel(xtriple.getKey().getMatchLabel(), xscope.getLeft());
                    }
                    if (!descListHelper.isEmpty()) {
                        assert matchLabel != null;
                        descListHelper.trimToSize();
                        descList.addAll(xtriple.getKey().getTree(), matchLabel, descListHelper);
                    }
                    if (!cousinListHelper.isEmpty()) {
                        assert matchLabel != null;
                        cousinListHelper.trimToSize();
                        cousinList.addAll(xtriple.getKey().getTree(), matchLabel, cousinListHelper);
                    }
                }
            }
            Map<Element, ScopeList> result = new HashMap<>();
            if (isFrequent(descList)) {
                Element e = new Element(y.getKey().getLabel(), n);
                result.put(e, descList);
            }
            if (isFrequent(cousinList)) {
                result.put(y.getKey(), cousinList);
            }
            return result;
        }
    }

    private int computeInducedSupport(TreeString s, ScopeList proofs) {
        boolean[] groupFrequencies = new boolean[maxGroup + 1];
        int freq = 0;
        for (Map.Entry<ScopeList.Key, List<Scope>> x : proofs.mapIterator()) {
            int g = x.getKey().getTree().getGroup();
            if (groupFrequencies[g]) {
                continue;
            }
            for (Scope proof : x.getValue()) {
                if (x.getKey().getTree().isInducedSubtree(s, x.getKey().getMatchLabel(), proof.getLeft())) {
                    groupFrequencies[g] = true;
                    freq++;
                    break;
                }
            }
        }
        return freq;
    }

    private int computeEmbeddedSupport(ScopeList list) {
        boolean[] groupFrequencies = new boolean[maxGroup + 1];
        int freq = 0;
        for (Tree t : list.getTrees()) {
            int g = t.getGroup();
            if (!groupFrequencies[g]) {
                groupFrequencies[g] = true;
                freq++;
            }
        }
        return freq;
    }

    private boolean isFrequent(ScopeList list) {
        boolean[] groupFrequencies = new boolean[maxGroup + 1];
        int freq = 0;
        for (Tree t : list.getTrees()) {
            int g = t.getGroup();
            if (!groupFrequencies[g]) {
                groupFrequencies[g] = true;
                freq++;
                if (freq >= minSupport) {
                    return true;
                }
            }
        }
        return false;
    }
}
