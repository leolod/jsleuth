package put.semantic.jsleuth;

public interface BasicTreeString extends Iterable<Integer> {

    public int size();

    public int get(int n);

}
