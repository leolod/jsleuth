package put.semantic.jsleuth;

public class Scope {

    private final int left, right;

    public Scope(int left, int right) {
        this.left = left;
        this.right = right;
    }

    public int getLeft() {
        return left;
    }

    public int getRight() {
        return right;
    }

    @Override
    public String toString() {
        return String.format("[%d,%d]", left, right);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Scope)) {
            return false;
        }
        Scope other = (Scope) obj;
        return this.left == other.left && this.right == other.right;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.left;
        hash = 79 * hash + this.right;
        return hash;
    }

    public boolean contains(Scope other) {
        return left < other.left && other.right <= right;
    }

    public boolean before(Scope other) {
        return right < other.left;
    }

}
